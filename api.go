package productai

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type ImageInfo struct {
	URL string
	Meta string
}

type Client struct {
	User string
	SecretKey string
	AccessKeyId string
	ImageSetId string
	ServiceId string
}

type request struct {
	URI string
	Form url.Values
	SignForm bool
	UserHeader bool
}

func (c *Client) do(r *request) (string,error) {
	h := getAuthHeader(c.AccessKeyId)
	form := r.Form
	if !r.SignForm { form = nil }
	signature := sign(h, form, c.SecretKey)
	h.Set("x-ca-signature", signature)
	
	req,err := http.NewRequest("POST",
		"https://api.productai.cn" + r.URI,
		strings.NewReader(r.Form.Encode()))
	if err != nil { panic(err) }
	req.Header.Set("Content-Type",
		"application/x-www-form-urlencoded")
	for key,vals := range h {
		req.Header[key] = vals
	}
	if r.UserHeader {
		req.Header.Set("pai_user_id", c.User)
	}
	
	resp,err := http.DefaultClient.Do(req)
	if resp != nil { defer resp.Body.Close() }
	if err != nil { return err.Error(), err }
	
	body,err := ioutil.ReadAll(resp.Body)
	if err != nil { return err.Error(), err }
	
	return fmt.Sprintf("[%s]%s", resp.Status, string(body)), nil
}

func (c *Client) imageSet(cap string, images []*ImageInfo) (string,error) {
	buf := &bytes.Buffer{}
	for _,img := range images {
		meta := img.Meta
		meta = strings.Replace(meta, ",", "", -1)
		meta = strings.Replace(meta, "\n", "", -1)
		buf.Write([]byte(img.URL + "," + meta + "\n"))
	}
	data := string(buf.Bytes())
	
	return c.do(&request{
		URI : "https://api.productai.cn/image_sets/_0000014/" + c.ImageSetId,
		Form : url.Values{
			cap : []string{data},
		},
		UserHeader : true,
	})
}

func (c *Client) ImageSetAdd(images []*ImageInfo) (string,error) {
	return c.imageSet("urls_to_add", images)
}

func (c *Client) ImageSetDel(images []*ImageInfo) (string,error) {
	return c.imageSet("urls_to_delete", images)
}
