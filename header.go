package productai

import (
	"crypto/rand"
	"fmt"
	"net/http"
	"strings"
	"time"
)

func getAuthHeader(accessKeyId string) http.Header {
	return http.Header{
		"x-ca-accesskeyid" : []string{"accessKeyId"},
		"x-ca-version" : []string{"1"},
		"x-ca-timestamp" : []string{fmt.Sprint(time.Now().Unix())},
		"x-ca-signaturenonce" : []string{signaturenonce()},
		"requestmethod" : []string{"post"},
	}
}

func signaturenonce() string {
	return strings.Join([]string{randHex(8), randHex(4),
		randHex(4), randHex(12)}, "-")
}

func randHex(size int) string {
	buffer := make([]byte, size / 2 + size % 2)
	_,err := rand.Read(buffer)
	if err != nil { panic(err) }
	return fmt.Sprintf("%x", buffer)[:size]
}
