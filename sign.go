package productai

import (
	"encoding/base64"
	"crypto/sha1"
	"crypto/hmac"
	"net/http"
	"net/url"
	"sort"
)

func sign(header http.Header, form url.Values, secretKey string) string {
	params := url.Values{}
	if header != nil {
		for key,vals := range header {
			for _,val := range vals {
				params.Add(key, val)
			}
		}
	}
	if form != nil {
		for key,vals := range form {
			for _,val := range vals {
				params.Add(key, val)
			}
		}
	}
	
	//组织签名串，不用params.Encode()，因为不需要URLEncoded格式
	signData := ""
	keys := []string{}
	for key,_ := range params {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _,key := range keys {
		for _,val := range params[key] {
			if len(signData) > 0 {
				signData += "&"
			}
			signData += (key + "=" + val)
		}
	}
	
	hash := hmac.New(sha1.New, []byte(secretKey))
	signBytes := hash.Sum([]byte(signData))
	return base64.StdEncoding.EncodeToString(signBytes)
}
